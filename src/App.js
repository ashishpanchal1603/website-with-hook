import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Registration from './components/registration/Registration'
import Login from './components/login/Login'
import Dashboard from './components/dashboard/Dashboard'
function App () {
  return (
    <>
    <Router>
      <Routes>
        <Route path='/' element={<Login/>}/>
        <Route path='/registration' element={<Registration/>}/>
        <Route path='/dashboard' element={<Dashboard/>}/>
      </Routes>
    </Router>
    </>
  )
}

export default App
