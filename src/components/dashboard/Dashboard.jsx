import React, {} from 'react'
import { Link } from 'react-router-dom'
import Logout from '../logout/Logout'
function Dashboard () {
  const getEmail = sessionStorage.getItem('email')
  const getPassword = sessionStorage.getItem('password')
  console.log('email', getEmail)
  console.log('getPwd', getPassword)

  return (
    <>

    <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>Email</th>
              <th>Password</th>
              <th className='btn'><button ><Link to="/">logout <Logout/></Link></button></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{getEmail}</td>
              <td>{getPassword}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  )
}

export default Dashboard
